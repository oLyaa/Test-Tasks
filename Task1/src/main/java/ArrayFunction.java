public class ArrayFunction {

    /**
     * Function takes an array of integers,
     * and returns integer from the longest recurring sequence within this array.
     * @param arr array of integers
     * @return -1 if array is empty, otherwise return found result
     */
    Integer longestSequence( int[] arr ) {
        if (arr.length==0) return -1;   /// return -1 if Integer Array is empty

        int maxINT=-1, maxCNT=0;   /// max found Integer value and its recurring sequence count

        /// get first value
        int curINT=arr[0];  /// current checked value
        int curCNT=1;       /// and its count

        for (int i=1; i<arr.length; i++) {
            /// same value
            if (curINT==arr[i]) curCNT++;

            /// changed value
            else {
                /// mark first found longest sequence (next one with same length is ignored)
                if (maxCNT<curCNT) {
                    maxINT=curINT;
                    maxCNT=curCNT;
                }
                /// get next value
                curINT=arr[i];
                curCNT=1;
            }
        }
        /// mark last sequence (if needed)
        if (maxCNT<curCNT) maxINT=curINT;

        return  maxINT;
    }



}
