import java.util.Random;
import static org.junit.Assert.*;

public class ArrayFunctionTest {

    private int[] createArray(int maxVal, int maxCount, int intArrLNG, boolean toLOG) {
        Random rand = new Random();
        int cnt, val, prevVal=-1;

        int ind=0;  // keeps nex value
        int[] arr = new int[intArrLNG];
        while (ind<arr.length) {
            cnt=rand.nextInt(maxCount) + 1;
            if (cnt>(arr.length -ind)) cnt=arr.length -ind;

            val = rand.nextInt(maxVal) + 1;
            if (val==prevVal) {
                if (val==1) val+=1;
                else val-=1;
            }

            for (int i=1; i<=cnt; i++) {
                if (toLOG) System.out.print( Integer.toString(val)+"," );
                arr[ind]=val;
                ind++;
            }
            if (toLOG) System.out.println(" "+Integer.toString(val)+"x"+Integer.toString(cnt) );
            prevVal=val;
        }
        return arr;
    }

    @org.junit.Test
    public void longestSequence() {
        int[] arr = new int[0];
        ArrayFunction fnc = new ArrayFunction();
        assertTrue("Empty array should return -1", fnc.longestSequence(arr) == -1);

        /// for Visual test in console
        arr = createArray(99,100,1000, true);
        System.out.println("---------------------------------");

        int longest=fnc.longestSequence(arr);
        System.out.println("The longest recurring sequence = "+ longest);
    }
}