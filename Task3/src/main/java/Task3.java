import java.util.*;
import static java.lang.System.*;

public class Task3 {

    static final String ERROR_MSG_SYMBOLS = "Incorrect symbol(s)\n";
    static final String ERROR_MSG_EMPTY = "Empty string\n";
    static final String MSG_BALANCED = "Line is balanced\n";
    static final String MSG_NOT_BALANCED = "Line is not balanced\n";

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.println("Program will determine if the parentheses (), the brackets [], and the braces, in a string are balanced.");
        System.out.println("Please, insert a line consisting of these symbols only.\n");
        String txt;
        while (true) {
            System.out.println("To quit program - type 'exit'");
            System.out.print("New line:");
            txt = sc.nextLine();
            /// check if exit command was inputted
            if (txt.toLowerCase().equals("exit")) exit(0);

            try {
                System.out.println( checkBalance(txt) );
            } catch(Exception e){
                System.out.println(ERROR_MSG_SYMBOLS);
            }
        }
    }

    static String checkBalance(String line){
        /// if empty line
        if (line.equals("")) return ERROR_MSG_EMPTY;

        Map<String, String> map = new Hashtable<String, String>();
        map.put("{", "}");
        map.put("[", "]");
        map.put("(", ")");
        String goodSymbols = "{}()[]";

        String element;
        List<String> waitFor = new LinkedList<String>();
        for(int i=0; i<line.length(); i++ ){
            String keyElement = Character.toString( line.charAt(i) );
            /// check for incorrect symbols
            if (!goodSymbols.contains(keyElement)) return ERROR_MSG_SYMBOLS;

            /// found "open" symbol... add to waitFor Stack "close" symbol
            if(map.containsKey(keyElement)){
                element = map.get(keyElement);
                ((LinkedList<String>) waitFor).addLast(element);
            } else {
            /// if no symbol is waiting for (not balanced on first symbol)
                if (waitFor.size()==0) return MSG_NOT_BALANCED;
            /// not the last symbol in waitFor stack
                if ( !((LinkedList<String>) waitFor).getLast().equals( keyElement )) return MSG_NOT_BALANCED;
            /// correct symbol found, delete it from stack
                ((LinkedList<String>) waitFor).removeLast();
            }
        }  /// for
        return MSG_BALANCED;
    }



}
