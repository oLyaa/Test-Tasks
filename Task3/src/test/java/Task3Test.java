import static org.junit.Assert.*;

public class Task3Test {

    @org.junit.Test
    public void TestCheckBalance() {
        assertEquals("Empty line", Task3.ERROR_MSG_EMPTY, Task3.checkBalance(""));
        assertEquals("Exit command", Task3.ERROR_MSG_SYMBOLS, Task3.checkBalance("exit"));
        assertEquals("random symbols", Task3.ERROR_MSG_SYMBOLS, Task3.checkBalance("fgdfg"));
        assertEquals("random symbols and brackets", Task3.ERROR_MSG_SYMBOLS, Task3.checkBalance("(e)"));

        assertEquals("Unbalanced line", Task3.MSG_NOT_BALANCED, Task3.checkBalance("{{)(}}"));
        assertEquals("Unbalanced line", Task3.MSG_NOT_BALANCED, Task3.checkBalance("({)}"));

        assertEquals("Balanced line", Task3.MSG_BALANCED, Task3.checkBalance("{([])}"));
        assertEquals("Balanced line", Task3.MSG_BALANCED, Task3.checkBalance("{()}[[{}]]"));
    }
}