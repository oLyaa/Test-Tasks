import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

public class Task2Test {

    @Test
    public void TestDrawTiangle() {
        assertFalse("lower then 1 - Incorrect Value", Task2.drawTiangle(0));
        assertFalse("Higherr then 999 - Incorrect Value", Task2.drawTiangle(1000));
        Random rand = new Random();
        int baseLength= rand.nextInt(999)+1;
        assertTrue("Check with "+Integer.toString(baseLength)+" base length", Task2.drawTiangle(baseLength));
        baseLength= rand.nextInt(999)+1;
        assertTrue("Check with "+Integer.toString(baseLength)+" base length", Task2.drawTiangle(baseLength));
        baseLength= rand.nextInt(999)+1;
        assertTrue("Check with "+Integer.toString(baseLength)+" base length", Task2.drawTiangle(baseLength));
        baseLength= rand.nextInt(999)+1;
        assertTrue("Check with "+Integer.toString(baseLength)+" base length", Task2.drawTiangle(baseLength));
        baseLength= rand.nextInt(999)+1;
        assertTrue("Check with "+Integer.toString(baseLength)+" base length", Task2.drawTiangle(baseLength));
    }
}