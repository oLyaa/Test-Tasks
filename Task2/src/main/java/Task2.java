import java.util.Scanner;
import static java.lang.System.*;

public class Task2 {

    private static final String ERROR_INPUT="Incorrect value\n";

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("This Program will draw a triangle in ASCII graphics.");
        String txt;
        while (true) {
            System.out.println("To quit program - type 'exit'");
            System.out.print("New Base’s length:");

            txt = sc.nextLine();
            /// check if exit command was inputted
            if (txt.toLowerCase().equals("exit")) exit(0);

            try {
                /// check Input value
                int checkValue = Integer.parseInt(txt);
                /// Draw Triangle with selected base length
                if (!drawTiangle(checkValue)) {
                    System.out.println(ERROR_INPUT);
                }
            } catch(Exception e){
                System.out.println(ERROR_INPUT);
            }
        }
    }

    /// draw line => spaces + ***** + spaces
    private static void drawLine(int baseVal,String levelVal){
        /// count of spaces to add at both sides
        int cnt = (baseVal-levelVal.length())/2;
        /// string with spaces
        String spaces;
        if (cnt>0) spaces = new String(new char[cnt]).replace('\0', ' ');
        else spaces = "";
        /// result line
        System.out.println( spaces + levelVal + spaces );
    }

    /// draw whole triangle
    static boolean drawTiangle(int baseVal){
        ///  set max input value, just to not overload console
        if ((baseVal<1) || (baseVal>999)) return false;
        String levelVal;
        /// create top string regarding odd or even base length
        if (baseVal%2==0) levelVal="**";
        else levelVal="*";
        /// draw top line
        drawLine(baseVal, levelVal);

        while (baseVal>levelVal.length()) {
            levelVal+="**";
            /// draw next line
            drawLine(baseVal, levelVal);
        }
        System.out.println();  // empty line before next request for base's length
        return true;
    }


}
